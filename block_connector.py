#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2021] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#
#
# #############################################################################
#  Block Connector - An Inkscape Extension
#  Appears in 'Extensions>Arrange
#  Just a simple example of shapes can be connected with Inkscape connectors
#  Connections follow the selection order
#  Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to inklinea.block_connector.noprefs
#  For shortcut triggered objects to new layer with last settings
#  Requires Inkscape 1.1+ -->
# #############################################################################

import inkex

from lxml import etree

# Could not find simplestyle, found this instead in extensions repo
def format_style(my_style):
    """Format an inline style attribute from a dictionary"""
    return ";".join([att + ":" + str(val) for att, val in my_style.items()])

def make_connection(self, start_object, end_object, connector_type='polyline', connector_curvature=0):

    parent = self.svg.get_current_layer()

    style = {'stroke': 'black',
             'stroke-width': '0.5',
             'fill': 'none',
             'stroke-dasharray': '1,1'
             }

    attribs = {
        'style': format_style(style),
        'd': ''
    }

    my_connector = etree.SubElement(parent, inkex.addNS('path', 'svg'), attribs)

    my_connector.set('inkscape:connector-type', connector_type)
    my_connector.set('inkscape:connector-curvature', str(connector_curvature))
    my_connector.set('inkscape:connection-start', f'#{start_object.get_id()}')
    my_connector.set('inkscape:connection-end', f'#{end_object.get_id()}')

    return my_connector

class MyFirstExtension(inkex.EffectExtension):

    def effect(self):

        selection_list = self.svg.selected

        if len(selection_list) < 2:
            return

        target_block_index = 1

        for current_block_index in range(0, len(selection_list)-1):

            make_connection(self, selection_list[current_block_index], selection_list[target_block_index])

            target_block_index += 1

if __name__ == '__main__':
    MyFirstExtension().run()
