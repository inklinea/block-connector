# Block Connector

Block Connector - An Inkscape 1.1+ extension

        # #############################################################################
        #  Block Connector - An Inkscape Extension
        #  Appears in 'Extensions>Arrange
        #  Just a simple example of shapes can be connected with Inkscape connectors
        #  Connections follow the selection order
        #  Assign a shortcut in Inkscape Edit>Preferences>Interface>Keyboard to inklinea.block_connector.noprefs
        #  For shortcut triggered objects to new layer with last settings
        #  Requires Inkscape 1.1+ -->
        # #############################################################################
